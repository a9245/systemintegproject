//Callbacks

// const arrayOfNumbers = [1,2,3,4,4];

// const multiplyByTwo = (value) =>{
//     return value*2;
// }
// console.log(arrayOfNumbers.map(multiplyByTwo));

const generateRandomInteger = () =>{
    return new Promise ((resolve, reject) => {
        setTimeout(() =>{
            const randomInt = parseInt(Math.random() * 100);
            if(randomInt % 2 === 0){
                resolve(randomInt);
            }
            else{
                reject(randomInt);
            }
        },2000);
    });
};

const divideFurther = (value) =>{
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            value = value/2;
            if(value %2 ===0){
                resolve(randomInt);
            }
            else{
                reject(randomInt);
            }
        },1000);
    });
};


console.log("generating random number");
const promiseObject = generateRandomInteger();
promiseObject.then(value => console.log(`Yes ${value} is even`), (value=> console.error(`Aw ${value} is odd`)
));

console.log("waiting...");