<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=devices-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Image Slider in HTML/CSS</title>
    </head>
    <body>
        <!--Image Slider Start-->
        <div class="sliders">
            <div class="slides">
                <!--Radio Button Start-->
                <input type="radio" name="radio-btn" id="radio1">
                <input type="radio" name="radio-btn" id="radio2">
                <input type="radio" name="radio-btn" id="radio3">
                <input type="radio" name="radio-btn" id="radio4">
                <!--Radio Button Close-->

                <!--Image Start-->
                <div class="slide first">
                    <img src="Apartment Bedroom.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="Apartment Dining+Living Room.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="apartmentDining.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="livingRoom4.jpg" alt="">
                </div>
                <!--Image Close-->
                <!--Automatic Navigation Start-->
                <div class="navigation-auto">
                    <div class="auto-btn-1"></div>
                    <div class="auto-btn-2"></div>
                    <div class="auto-btn-3"></div>
                    <div class="auto-btn-4"></div>
                </div>
                 <!--Automatic Navigation Closed-->
            </div>
            <!---->
            <!--Manual Navigation Start-->
            <div class="navigation-mannual">
                <label for="radio1" class="mannual-btn"></label>
                <label for="radio2" class="mannual-btn"></label>
                <label for="radio3" class="mannual-btn"></label>
                <label for="radio4" class="mannual-btn"></label>
            </div>
            <!--Manual Navigation Close-->

        </div>
        <!--Image Slider Close-->

        <script>
            var counter = 1;
            setInterval(function(){
                document.getElementById('radio'+counter).checked = true;
                counter++;

                if(counter>4){
                    counter = 1;
                }
            },5000)
        </script>
    </body>
</html>



body{
    margin: 0;
    padding: 0;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
}
.sliders{
    width: 800px;
    height: 500px;
    border-radius: 10px;
    overflow: hidden;
}
.slides{
    width: 500%;
    height: 500px;
    display: flex;
}
.slides input{
    display: none;
}
.slide{
    width: 20%;
    transition: 2s;
}
.slide img{
    width: 800px;
    height: 500px;
}


 .navigation-mannual{
     position: absolute;
     width: 800px;
     margin-top: -40px;
     display: flex;
     justify-content: center;
 }
 .mannual-btn{
     border: 2px solid #ccc;
     padding: 5px;
     border-radius: 10px;
     cursor: pointer;
     transition: 1s;
 }

 .mannual-btn:not(:last-child){
     margin-right: 40px;
 }

 .mannual-btn:hover{
     background-color: #ccc;
 }

 #radio1:checked ~ .first{
     margin-left: 0;
 }
 #radio2:checked ~ .first{
    margin-left: -20%;
}
#radio3:checked ~ .first{
    margin-left: -40%;
}
#radio4:checked ~ .first{
    margin-left: -60%;
}

/*Automatic Navigation*/

.navigation-auto{
    position: absolute;
    display: flex;
    width: 800px;
    justify-content: center;
    margin-top: 460px;
}

.navigation-auto div{
    border: 2px solid #333;
    padding: 5px;
    border-radius: 10px;
    transition: 1s;
}

.navigation-auto div:not(:last-child){
    margin-right: 40px;
}

#radio1:checked ~.navigation-auto .auto-btn-1{
    background-color: #ccc;
}
#radio2:checked ~.navigation-auto .auto-btn-1{
    background-color: #ccc;
}
#radio3:checked ~.navigation-auto .auto-btn-1{
    background-color: #ccc;
}
#radio4:checked ~.navigation-auto .auto-btn-1{
    background-color: #ccc;
}