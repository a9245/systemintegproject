<?php
    include 'config.php';
    if(isset($_POST['submit'])){
        // $id=$row['id'];
        $Full_Name=$_POST['Full_Name'];
        $Phone_Number=$_POST['Phone_Number'];
        $Email=$_POST['Email'];
        $ValidIDNum=$_POST['ValidIDNum'];
        $RoomNum=$_POST['RoomNum'];
        
        $sql="insert into renterinformation (Fullname, PhoneNumber, Email, ValidIDNumber, RoomNumberSelected) 
        values ('$Full_Name','$Phone_Number','$Email','$ValidIDNum','$RoomNum')";

        $result=mysqli_query($conn,$sql);
        if($result){
            header('location:display.php');
        }else{
            die(mysqli_error($conn));
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Insert Renter Information</title>
</head>
<body>
    <div class="container">
        <form method="post">
            <div class="form">
                <label>Full Name:</label>
                <input class="input" type="text" placeholder="Enter Full Name" autocomplete="off" name=Full_Name>
            </div>
            <div class="form">
                <label>Phone Number:</label>
                <input class="input" type="text" placeholder="Enter Phone Number" autocomplete="off" name=Phone_Number>
            </div>
            <div class="form">
                <label>Email:</label>
                <input class="input" type="text" placeholder="Enter Email Address" autocomplete="off" name=Email>
            </div>  
            <div class="form">
                <label>Valid ID Number:</label>
                <input class="input" type="text" placeholder="Enter Valid ID Number" name=ValidIDNum>
            </div>  
            <div class="form">
                <label>Room Number Selected:</label>
                <input class="input" type="text" placeholder="" autocomplete="off" name=RoomNum>
            </div>      
            <button type="submit" class="btn_insert" name="submit" header="display.php" >Insert</button>
    </div>
    
</body>
</html>